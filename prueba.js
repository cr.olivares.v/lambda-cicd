const {handler} = require('./index');

(async () => {
    console.log(await handler());
})();