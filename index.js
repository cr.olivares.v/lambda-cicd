const axios = require('axios');

/* exports.handler = async event => {
    const response = {
        statusCode: 200,
        body: JSON.stringify('Hello from Lambda staging!'),
    };
    return response; 
} */
exports.handler = async event => (
{
    statusCode: 200,
    body: await axios.get('https://jsonplaceholder.typicode.com/todos')
                     .then(response => response.data.filter(e => e.completed === true))
                     .then(response => JSON.stringify(response))
                     .catch(error => [])
});
/* exports.handler = async event => await axios.get('https://jsonplaceholder.typicode.com/todos')
                                            .then(response => response.data.filter(e => e.completed === true))
                                            .catch(error => []); */


/*exports.handler = async event => await axios.get('https://jsonplaceholder.typicode.com/albums')
                                            .then(response => response.data.filter(e => e.userId === 1))
                                            .catch(error => error);*/